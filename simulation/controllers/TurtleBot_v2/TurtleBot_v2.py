from controller import Robot, Motor, Lidar

TIME_STEP = 64
robot = Robot()

# safety distance
s = 0.5

# initialize Lidar
lid = robot.getLidar('TurtleBotLidar')
lid.enable(TIME_STEP)

wheels = []
wheelsNames = ['wheel1', 'wheel2']

for i in range(2):
    wheels.append(robot.getMotor(wheelsNames[i]))
    wheels[i].setPosition(float('inf'))
    wheels[i].setVelocity(0.0)

avoidObstacleCounter = 0

while robot.step(TIME_STEP) != -1:
    leftSpeed = 1.0
    rightSpeed = 1.0
    if avoidObstacleCounter > 0:
        avoidObstacleCounter -= 1
        leftSpeed = 1.0
        rightSpeed = -1.0
    else:  # read sensors
        #print("LIDAR RANGE IMAGE:")
        lidarIm = lid.getRangeImage()
        for i in range(45):
            if lidarIm[i] and lidarIm[359-i]:
                if lidarIm[i] < s or lidarIm[359-i] < s:
                    print ("s: {} , r: {} / {}".format(s,lidarIm[i],lidarIm[359-i]))
                    avoidObstacleCounter = 100
        #print(len(lidarIm))
        #print(lidarIm)
        
    wheels[0].setVelocity(leftSpeed)
    wheels[1].setVelocity(rightSpeed)