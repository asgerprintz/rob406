"""my_first_controller controller."""

# You may need to import some classes of the controller module. Ex:
#  from controller import Robot, Motor, DistanceSensor
from controller import Robot, Motor

# create the Robot instance.
robot = Robot()

TIME_STEP = 64
MAX_SPEED = 6.28
ts = 0.9
# get the time step of the current world.
timestep = int(robot.getBasicTimeStep())

# get the motor devices
leftMotor = robot.getMotor('left wheel motor')
rightMotor = robot.getMotor('right wheel motor')
# set the target position of the motors

##The robot will move using its maximum speed for a while
## and then stop once the wheels have rotated of 10 radians.
#leftMotor.setPosition(10.0)
#rightMotor.setPosition(10.0)

leftMotor.setPosition(float('inf'))
rightMotor.setPosition(float('inf'))

# set up the motor speeds at 10% of the MAX_SPEED.
leftMotor.setVelocity(ts * MAX_SPEED)
rightMotor.setVelocity(ts * MAX_SPEED)

while robot.step(TIME_STEP) != -1:
   pass
