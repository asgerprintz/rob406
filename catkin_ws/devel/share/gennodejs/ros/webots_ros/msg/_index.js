
"use strict";

let Int32Stamped = require('./Int32Stamped.js');
let Int8Stamped = require('./Int8Stamped.js');
let RadarTarget = require('./RadarTarget.js');
let RecognitionObject = require('./RecognitionObject.js');
let BoolStamped = require('./BoolStamped.js');
let Float64Stamped = require('./Float64Stamped.js');
let StringStamped = require('./StringStamped.js');

module.exports = {
  Int32Stamped: Int32Stamped,
  Int8Stamped: Int8Stamped,
  RadarTarget: RadarTarget,
  RecognitionObject: RecognitionObject,
  BoolStamped: BoolStamped,
  Float64Stamped: Float64Stamped,
  StringStamped: StringStamped,
};
