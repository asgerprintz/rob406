# CMake generated Testfile for 
# Source directory: /home/asger/rob406/catkin_ws/src
# Build directory: /home/asger/rob406/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("turtlebot3/turtlebot3")
subdirs("turtlebot3_msgs")
subdirs("turtlebot3/turtlebot3_navigation")
subdirs("openai_ros/openai_ros")
subdirs("turtlebot3/turtlebot3_bringup")
subdirs("turtlebot3/turtlebot3_example")
subdirs("turtlebot3/turtlebot3_slam")
subdirs("turtlebot3/turtlebot3_teleop")
subdirs("webots_ros")
subdirs("turtlebot3/turtlebot3_description")
